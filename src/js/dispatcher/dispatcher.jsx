/* jslint esnext:true */
import {Dispatcher} from 'flux';

/**
 * Disparcher instance
 */
export default new Dispatcher();