/* jslint esnext: true */

import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute } from 'react-router';
import history from './utils/history';
import App from './components/App';

// pages
import Search from './components/Search';
import Home from './components/Home';
import Signin from './components/Auth/Signin';
import Signup from './components/Auth/Signup';
import Account from './components/Account';
import EditEmail from './components/Account/EditEmail';
import EditPassword from './components/Account/EditPassword';
import EditProfile from './components/User/EditProfile';
import Dashboard from './components/Dashboard';
import Review from './components/Review';

// actions
import AuthActions from './actions/AuthActions';

// AuthActions.checkAuth();

class Main extends React.Component{

    componentWillMount(){
        AuthActions.checkAuth();
    }

    render(){
        return (
            <Router history={history} >
                <Route path="/" component={App} >
                    <IndexRoute component={Home} />
                    <Route path="/search" component={Search} />
                    <Route path="/signin" component={Signin} />
                    <Route path="/signup" component={Signup} />
                    <Route path="/dashboard" component={Dashboard} />

                    <Route path="/account" component={Account} />
                    <Route path="/account/email" component={EditEmail} />
                    <Route path="/account/password" component={EditPassword} />

                    <Route path="/reviews" component={Review} />

                    <Route path="/users/edit" component={EditProfile} />
                </Route>
            </Router>
        )
    }
}

ReactDOM.render(<Main />, document.getElementById('app'));