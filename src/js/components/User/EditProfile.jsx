/* jslint esnext: true */

import React from 'react';
import classNames from 'classnames';
import api from './../../utils/api';
import AuthStore from './../../stores/AuthStore';
import { Form, FormError, Input, Select, Textarea, Button } from  './../Common/Form';
import Loader from  './../Common/Loader';
import history from './../../utils/history';
import lg from './../../config/locale';
import View from  './../Common/View';

class EditProfile extends Form{

    constructor(props, context){
        super(props, context);
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentWillMount(){
        api.get('/user/', this.onGetUser.bind(this), this.onGetUserErr.bind(this));
    }

    componentDidMount(){
        // console.log('profile did');
        // if(!AuthStore.isLoggedIn()){
        //     history.pushState(null, '/signin');
        // }
    }


    onGetUser(response){
        this.refs.view.loaded();
        console.log(response);
    }

    onGetUserErr(response){
        this.refs.view.loaded();
        console.log(response);
    }
    

    onSubmit(e){
        e.preventDefault();
        if(this.validate()){
            this.clean();
            this.refs.button.loading();
        }
    }

    onChange(field, value){
        let data = {};
        data[field] = value;
        this.setState(data);
    }

    render(){

        return (
            <View ref="view" className="edit-profile content content--medium content--center">
                <h1>{lg.t('profile.edit')}</h1>

                <form onSubmit={this.onSubmit} ref="form" className="form pure-form pure-form-stacked" method="post">
                    
                    <FormError
                        ref="non_field_errors"
                        className="alert alert--error">
                    </FormError>

                    <div className="row edit-profile__avatar">
                        <div className="avatar avatar--xlarge">
                            <span className="icon--circle">
                                <i className="icon icon-camera"></i>
                            </span>
                            <img src="/img/avatar.png" />
                        </div>
                    </div>

                    <div className="row">
                        <div className="six columns">
                            <Input 
                            ref="first_name" 
                            name="first_name"
                            label={lg.t('first_name')} 
                            changeHandler={this.onChange} 
                            type="text" 
                            className="is-full"
                            placeholder={lg.t('first_name')} 
                            validations="isRequired,isAlpha" />
                        </div>

                        <div className="six columns">
                            <Input 
                            ref="last_name" 
                            name="last_name"
                            label={lg.t('last_name')} 
                            changeHandler={this.onChange} 
                            type="text" 
                            className="is-full"
                            placeholder={lg.t('last_name')}
                            validations="isRequired,isAlpha" />
                        </div>
                    </div>

                    <div className="row">
                        <div className="six columns">
                            <Input 
                            ref="birth_date" 
                            name="birth_date"
                            label={lg.t('birth_date')} 
                            changeHandler={this.onChange} 
                            type="text" 
                            className="is-full"
                            placeholder={lg.t('birth_date')} 
                            validations="isRequired" />
                        </div>

                        <div className="six columns">
                            <Select 
                            ref="gender" 
                            name="gender"
                            label={lg.t('gender')} 
                            changeHandler={this.onChange} 
                            className="is-full"
                            validations="isRequired">
                                <option></option>
                                <option>M</option>
                                <option>H</option>
                            </Select>
                        </div>
                    </div>

                    <div className="row">
                        <div className="twelve columns">
                            <Textarea 
                            ref="about" 
                            name="about"
                            label={lg.t('about.you')} 
                            changeHandler={this.onChange} 
                            className="is-full"
                            validations="isRequired" />
                        </div>
                    </div>

                    <Button ref="button" type="submit" className="pure-button-blue is-full">{lg.t('profile.edit')}</Button>
                
                </form>
                
            </View>
        )
    }
}

export default EditProfile;