/* jslint esnext: true */

import React from 'react';
import classNames from 'classnames';
import Loader from './Loader';

class View extends React.Component{

    constructor(props, context){
        super(props, context);
        this.state = {loading:true};
    }

    loading(){
        this.setState({loading:true});
    }

    loaded(){
        this.setState({loading:false});
    }

    render(){

        let classes = classNames(this.props.className, {
            'view': true,
        });

        let loaderClasses = classNames({
            'view__loader': true,
            'loader--show': this.state.loading
        });

        return (
            <div className={classes}>
                <Loader className={loaderClasses} />
                {this.props.children}
            </div>
        )
    }
}

export default View;