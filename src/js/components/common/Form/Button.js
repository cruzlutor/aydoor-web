/* jslint esnext: true */

import React from 'react';
import classNames from 'classnames';
import Loader from './../Loader';
import lg from './../../../config/locale';

class Button extends React.Component{

    constructor(props, context){
        super(props, context);
        this.state = {disabled: false, loading: false};
    }

    disabled(){
        this.setState({disabled:true});
    }

    enabled(){
        this.setState({disabled:false});
    }

    loading(){
        this.disabled()
        this.setState({loading:true});
    }

    loaded(){
        this.enabled();
        this.setState({loading:false});
    }

    render(){

        let classes = classNames(this.props.className, {
            'pure-button': true,
            'pure-button--loading': this.state.loading,
        });

        return (
            <button type={this.props.type} className={classes} disabled={this.state.disabled}>
                <Loader className="pure-button-loader" />
                <span className="pure-button-text">{this.props.children}</span>
            </button>
        )
    }
}

export default Button;