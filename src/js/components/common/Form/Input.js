/* jslint esnext: true */

import React from 'react';
import classNames from 'classnames';
import validator from '../../../utils/validator';
import lg from './../../../config/locale';
import Field from  './Field';

class Input extends Field{

    constructor(props, context){
        super(props, context);
    }

    render(){

        let classes = classNames({
            'form__control': true,
            'form__control--invalid': this.state.isError,
            // 'form__control--valid': this.state.isValid,
        });

        return (
            <div className={classes}>

                <label>{this.props.label}</label>
                <input
                    onChange={this.onChange}
                    className={this.props.className}
                    name={this.props.name}
                    type={this.props.type}
                    placeholder={this.props.placeholder}
                    value={this.state.value} />

                <div className="form__control__error">{this.state.error}</div>
            </div>
        );
    }
}

export default Input;
