/* jslint esnext: true */

import React from 'react';
import classNames from 'classnames';
import validator from '../../../utils/validator';
import lg from './../../../config/locale';

class Field2 extends React.Component{

    constructor(props, context){
        super(props, context);
        this.state = {isError:false, isValid:false, value:'', validations:this.props.validations.split(",")};
        this.onChange = this.onChange.bind(this);
        this.validate = this.validate.bind(this);
    }

    validate(){
        for(let value of this.state.validations){
            if(value){
                if(!validator[value](this.state.value)){
                    this.error('validator.' + value);
                    return false;
                }
            }
        }
        this.valid();
        return true;
    }

    valid(){
        this.setState({'error': null});
        this.setState({'isError': false});
        // this.setState({'isValid': true});
    }

    error(msg){
        this.setState({'error': lg.t(msg)});
        this.setState({'isError': true});
        // this.setState({'isValid': false});
    }

    onChange(event){
        /* set the current value */
        this.setState({'value': event.target.value});

        /* hide error message */
        this.setState({'isError': false});
        // this.setState({'isValid': false});

        /* fire the chage to the parent */
        this.props.changeHandler(this.props.name, event.target.value);
    }
}

export default Field2;
