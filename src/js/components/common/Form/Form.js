/* jslint esnext: true */

import React from 'react';

class Form extends React.Component{


    /**
     * Make input, textarea, select validations
     * @return {bool} true or false validation
     */
    validate(){
        let valid = true;
        for(let field in this.refs){
            if( this.refs[field].constructor.name === 'Input' || 
                this.refs[field].constructor.name === 'Select' || 
                this.refs[field].constructor.name === 'Textarea' ){
                if(!this.refs[field].validate()){
                    valid = false;
                }
            }
        }
        return valid;
    }


    /**
     * Clean non_fields_errors
     * @return {none}
     */
    clean(){
        if(this.refs.non_field_errors){
            this.refs.non_field_errors.error();
        }
    }


    /**
     * Parse server errors and pass to the respective input name
     * @param  {object} errors server json errors
     * @return {none}
     */
    parseErrors(errors){
        let _errors = [];
        for(let error of Object.keys(errors) ){
            _errors[error] = errors[error][0];
            if(this.refs[error]) this.refs[error].error(_errors[error]);
        }
    }
}

export default Form;
