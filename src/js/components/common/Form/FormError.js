/* jslint esnext: true */

import React from 'react';
import classNames from 'classnames';
import lg from './../../../config/locale';

class FormError extends React.Component{

    constructor(props, context){
        super(props, context);
        this.state = {error:''};
    }

    error(msg){
        this.setState({'error': lg.t(msg)});
    }

    render(){

        let classes = classNames(this.props.className, {
            'is-none': !this.state.error,
        });
        
        return (
            <div className={classes}>{this.state.error}</div>
        );
    }
}

export default FormError;
