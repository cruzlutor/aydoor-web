/* jslint esnext: true */

import React from 'react';
import classNames from 'classnames';


class Loader extends React.Component{

    constructor(props, context){
        super(props, context);
    }

    render(){

        let classes = classNames(this.props.className, {
            'loader': true,
        });

        return (
            <div className={classes}>
                <div></div>
                <div></div>
                <div></div>
            </div>
        )
    }
}

export default Loader;