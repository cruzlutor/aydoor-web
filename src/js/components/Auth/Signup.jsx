/* jslint esnext: true */

import React from 'react';
import classNames from 'classnames';
import SuggestPlaces from './../Common/Suggest/SuggestPlaces';
import AuthActions from  './../../actions/AuthActions';
import { Form, FormError, Input, Button } from  './../Common/Form';
import api from './../../utils/api';
import lg from './../../config/locale';

class Signup extends Form{

    constructor(props, context){
        super(props, context);
        this.state = {};
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onSubmit(e){
        e.preventDefault();
        if(this.validate()){
            this.refs.button.loading();
        }
    }

    onChange(field, value){
        let data = {};
        data[field] = value;
        this.setState(data);
    }

    render(){


        return (
            <div className="content is-center">

                <h1>{lg.t('sign_up')}</h1>
                
                <form onSubmit={this.onSubmit} ref="form" className="pure-form form content--small content--center" method="post">

                    <Input 
                        ref="first_name" 
                        name="first_name"
                        changeHandler={this.onChange} 
                        type="text" 
                        className="is-full"
                        placeholder={lg.t('first_name')} 
                        validations="isRequired,isAlpha" />

                    <Input 
                        ref="last_name" 
                        name="last_name"
                        changeHandler={this.onChange} 
                        type="text" 
                        className="is-full"
                        placeholder={lg.t('last_name')}
                        validations="isRequired,isAlpha" />

                    <Input 
                        ref="username" 
                        name="username"
                        changeHandler={this.onChange} 
                        type="text" 
                        className="is-full"
                        placeholder="Email" 
                        validations="isRequired,isEmail" />

                    <Input 
                        ref="password" 
                        name="password"
                        changeHandler={this.onChange} 
                        type="password" 
                        className="is-full"
                        placeholder="Password" 
                        validations="isRequired" />

                    <Button ref="button" type="submit" className="pure-button-blue is-full">{lg.t('sign_up')}</Button>

                </form>  
            </div>
        )
    }
}

export default Signup;