/* jslint esnext: true */

import React from 'react';
import classNames from 'classnames';
import history from './../../utils/history';
import AuthActions from  './../../actions/AuthActions';
import AuthStore from './../../stores/AuthStore';
import Actions from './../../constants/Actions';
import { Form, FormError, Input, Button } from  './../Common/Form';
import Loader from  './../Common/Loader';
import api from './../../utils/api';
import lg from './../../config/locale';

class Signin extends Form{

    constructor(props, context){
        super(props, context);
        
        this.state = {};

        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);

        this.onSignIn = this.onSignIn.bind(this);
        this.onSignInErr = this.onSignInErr.bind(this);
    }

    onSubmit(e){
        e.preventDefault();
        if(this.validate()){
            this.clean();
            this.refs.button.loading();
            api.post('/account/auth/', this.state, this.onSignIn, this.onSignInErr);
        }
    }

    onSignIn(response){
        this.refs.button.loaded();
        AuthActions.signIn(response.body);
        history.pushState(null, '/dashboard');
    }
    
    onSignInErr(response){
        this.refs.button.loaded();
        this.parseErrors(response.body);
        AuthActions.signInError(response.body);
    }
    
    onChange(field, value){
        let data = {};
        data[field] = value;
        this.setState(data);
    }

    render(){

        return (
            <div className="content is-center">

                <h1>{lg.t('sign_in')}</h1>

                <form onSubmit={this.onSubmit} ref="form" className="pure-form form content--small content--center" method="post">

                    <FormError
                        ref="non_field_errors"
                        className="alert alert--error">
                    </FormError>

                    <Input 
                        ref="username" 
                        name="username"
                        changeHandler={this.onChange} 
                        type="text" 
                        className="is-full"
                        placeholder="Email" 
                        validations="isRequired,isEmail" />

                    <Input 
                        ref="password" 
                        name="password"
                        changeHandler={this.onChange} 
                        type="password" 
                        className="is-full"
                        placeholder="Password" 
                        validations="isRequired" />
                
                    <Button ref="button" type="submit" className="pure-button-blue is-full">{lg.t('sign_in')}</Button>

                </form>

            </div>
        )
    }
}

export default Signin;