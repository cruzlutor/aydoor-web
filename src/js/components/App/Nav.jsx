/* jslint esnext: true */

import React from 'react';
import { Link } from 'react-router';
import history from './../../utils/history';
import classNames from 'classnames';
import AuthActions from './../../actions/AuthActions';
import AuthStore from './../../stores/AuthStore';
import Actions from './../../constants/Actions';
import lg from './../../config/locale';

class Nav extends React.Component{

    constructor(props, context){
        super(props, context);
        this.state = {positive: false, isLoggedIn:false, dropDown:false};
        this.onSignIn = this.onSignIn.bind(this);
        this.signOut = this.signOut.bind(this);
        this.onSignOut = this.onSignOut.bind(this);
    }

    componentWillMount(){
        //set current loggin status
        this.setState({isLoggedIn:AuthStore.isLoggedIn()});

        //add listeners
        AuthStore.addListener(Actions.SIGNIN, this.onSignIn);
        AuthStore.addListener(Actions.SIGNOUT, this.onSignOut);
    }

    componentWillUnmount(){
        //remove listeners
        AuthStore.removeListener(Actions.SIGNIN, this.onSignIn);
        AuthStore.removeListener(Actions.SIGNOUT, this.onSignOut);
    }

    componentDidMount(){
        history.listen( (nextState) => {
            this.setState(
                {positive: (nextState.pathname != '/') }
            );
        });
    }

    onSignIn(){
        this.setState({'isLoggedIn': true});
    }

    signOut(){
        AuthActions.signOut();
    }

    onSignOut(){
        this.setState({'isLoggedIn': false});
        history.pushState(null, '/');
    }

    openDropDown(){
        this.setState({'dropDown': true});
    }

    closeDropDown(){
        this.setState({'dropDown': false});
    }

    render(){
        let classes = classNames({
            'main-nav': true,
            'main-nav--positive': this.state.positive,
        });

        let dropDownClasses = classNames({
            'dropdown': true,
            'dropdown--right': true,
            'dropdown--open': this.state.dropDown,
            'main-nav__dropdown': true,
        });

        if(this.state.isLoggedIn){
            return (
                <nav className={classes}>

                    <div className="main-nav__item pull-left">
                        <Link className="main-nav__logo" to={'/'}></Link>
                    </div>

                    <div onMouseLeave={this.closeDropDown.bind(this)} className="main-nav__item pull-right is-desktop main-nav__item--dropdown">
                        <div onClick={this.openDropDown.bind(this)} className="main-nav__link">
                            <div className="main-nav__avatar avatar avatar--small">
                                <img src="/img/avatar.png" />
                            </div>
                        </div>

                        <div className={dropDownClasses}>
                            <div onClick={this.closeDropDown.bind(this)} className="dropdown__content">
                                <Link className="dropdown__item" to={'/dashboard'}>{lg.t('dashboard')}</Link>
                                <Link className="dropdown__item" to={'/users/edit'}>{lg.t('profile.edit')}</Link>
                                <Link className="dropdown__item" to={'/account'}>{lg.t('account.my')}</Link>
                                <Link className="dropdown__item" to={'/reviews'}>{lg.t('review._p')}</Link>
                                <div onClick={this.signOut} className="dropdown__item">{lg.t('sign_out')}</div>
                            </div>
                        </div>
                    </div>

                    <div className="main-nav__item pull-right is-desktop">
                        <Link className="main-nav__link" to={'/'}>
                            {lg.t('help')}
                        </Link>
                    </div>

                    <div className="main-nav__item pull-right is-desktop">
                        <Link className="main-nav__link" to={'/'}>
                            {lg.t('service._p')}
                        </Link>
                    </div>

                    <div className="main-nav__item pull-right is-desktop">
                        <Link className="main-nav__link" to={'/'}>
                            {lg.t('appointment._p')}
                        </Link>
                    </div>

                    <div className="main-nav__item pull-right is-desktop">
                        <Link className="main-nav__link" to={'/'}>
                            {lg.t('message._p')}
                        </Link>
                    </div>

                    <div className="main-nav__item pull-right is-mobile">
                        <a className="main-nav__link main-nav__burguer" href="#">
                            <i className="icon icon-nav"></i>
                        </a>
                    </div>

                </nav>
            )
        }else{
            return (
                <nav className={classes}>
                    <div className="main-nav__item pull-left"><Link className="main-nav__logo" to={'/'}></Link></div>

                    <div className="main-nav__item pull-right is-desktop"><Link className="main-nav__link" to={'/signin'}>{lg.t('sign_in')}</Link></div>
                    <div className="main-nav__item pull-right is-desktop"><Link className="main-nav__link" to={'/signup'}>{lg.t('sign_up')}</Link></div>
                    <div className="main-nav__item pull-right is-desktop"><Link className="main-nav__link" to={'/'}>{lg.t('home')}</Link></div>

                    <div className="main-nav__item pull-right is-mobile"><a className="main-nav__link main-nav__burguer" href="#"><i className="icon icon-nav"></i></a></div>
                </nav>
            )
        }
    }
}

export default Nav;