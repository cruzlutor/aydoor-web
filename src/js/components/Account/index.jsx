/* jslint esnext: true */

import React from 'react';
import AuthStore from './../../stores/AuthStore';
import { Link } from 'react-router';
import history from './../../utils/history';
import lg from './../../config/locale';

class Account extends React.Component{

    constructor(props, context){
        super(props, context);
        this.state = {};
    }

    componentWillMount(){
        if(!AuthStore.isLoggedIn()){
            history.pushState(null, '/signin');
        }
    }

    render(){
        return (
            <div className="account content content--medium content--center">
                <h1>{lg.t('account.my')}</h1>

                <div className="list">
                    <div className="list__item">
                        Email
                        <p>root@root.com</p>
                        <Link className="pure-button pure-button-blue pure-button--ghost list__item__right" to={'/account/email'}>
                            <span className="is-desktop">{lg.t('email.change')}</span>
                            <span className="is-mobile"><i className="icon-pencil"></i></span>
                        </Link>
                    </div>

                    <div className="list__item">
                        Password
                        <Link className="pure-button pure-button-blue pure-button--ghost list__item__right" to={'/account/password'}>
                            <span className="is-desktop">{lg.t('password.change')}</span>
                            <span className="is-mobile"><i className="icon-key"></i></span>
                        </Link>
                    </div>

                    <div className="list__item">
                        {lg.t('payment.settings')}
                        <Link className="pure-button pure-button-blue pure-button--ghost list__item__right" to={'/account/payment'}>
                            <span className="is-desktop">{lg.t('configure')}</span>
                            <span className="is-mobile"><i className="icon-credit-card"></i></span>
                        </Link>
                    </div>

                </div>

            </div>
        )
    }
}


export default Account;