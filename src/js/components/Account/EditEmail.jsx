/* jslint esnext: true */

import React from 'react';
import AuthStore from './../../stores/AuthStore';
import history from './../../utils/history';
import lg from './../../config/locale';

class EditEmail extends React.Component{

    constructor(props, context){
        super(props, context);
        this.state = {};
    }

    componentWillMount(){
    }

    componentDidMount(){
    }

    render(){
        return (
            <div className="content is-center">
                <h1>{lg.t('email.change')}</h1>

                <form className="form pure-form content--small content--center">

                    <input id="first-name" className="is-full" type="text" />

                    <button type="submit" className="pure-button pure-button-blue is-full">{lg.t('change')}</button>

                </form>
                
            </div>
        )
    }
}

export default EditEmail;