/* jslint esnext: true */

import React from 'react';
import AuthStore from './../../stores/AuthStore';
import history from './../../utils/history';
import lg from './../../config/locale';

class Dashboard extends React.Component{

    constructor(props, context){
        super(props, context);
        this.state = {};
    }

    componentWillMount(){
        if(!AuthStore.isLoggedIn()){
            history.pushState(null, '/signin');
        }
    }

    render(){
        return (
            <div>Dashboard</div>
        )
    }
}

export default Dashboard;