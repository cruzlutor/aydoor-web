/* jslint esnext: true */

import React from 'react';
import AuthStore from './../../stores/AuthStore';
import { Link } from 'react-router';
import history from './../../utils/history';
import lg from './../../config/locale';

class Review extends React.Component{

    constructor(props, context){
        super(props, context);
        this.state = {};
    }

    // componentWillMount(){
    //     if(!AuthStore.isLoggedIn()){
    //         history.pushState(null, '/signin');
    //     }
    // }

    render(){
        return (
            <div className="content content--medium content--center">
                <h1>{lg.t('review.comments')}</h1>

                <div className="list">

                </div>

            </div>
        )
    }
}


export default Review;