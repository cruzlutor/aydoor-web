/* jslint esnext:true */

import store from 'store';
import Dispatcher from './../dispatcher/Dispatcher';
import Actions from './../constants/Actions';

export default {

    /**
     * Use to check local token
     * @return {none}
     */
    checkAuth(){
        let token = store.get('token');
        if(token){
            Dispatcher.dispatch({
                actionType: Actions.SIGNIN,
                token: token
            });
        }
    },

    /**
     * Login method
     * @param  {object} data Data object from the server
     * @return {none}
     */
    signIn(data){
        Dispatcher.dispatch({
            actionType: Actions.SIGNIN,
            token: data.token
        });
    },

    /**
     * Login Error
     * @param  {data} data Error response
     * @return {none}
     */
    signInError(data){
        Dispatcher.dispatch({
            actionType: Actions.SIGNIN_ERROR,
            data: data
        });
    },

    /**
     * Logout function
     * @return {none}
     */
    signOut(){
        Dispatcher.dispatch({
            actionType: Actions.SIGNOUT
        });
    },
};