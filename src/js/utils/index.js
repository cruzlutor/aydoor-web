/* jslint esnext:true */

// import Dispatcher from './../dispatcher/Dispatcher';

export default {
    /**
     * Deprecated, not in use
     * Util for superagent request success & error flux dipatcher
     * @param  {object} request superagent request
     * @param  {string} onSuccess
     * @param  {string} onError
     * @return {none}
     
     * apiDispatcher(request, onSuccess, onError){
     *     request.end( (error, response) => {
     *         Dispatcher.dispatch({
     *             actionType: (!error) ? onSuccess : onError,
     *             data: (response) ? response.body : {}
     *         });
     *     });
     * }
     */
};