/* jslint esnext:true */

import request from 'superagent';

export default {

    _request(type, url, data, onSuccess, onError){
        
        let req = request(type, 'http://localhost:8000/api' + url);

        /* if data is a function, then change roles */
        if ('function' == typeof data){
            onError = onSuccess, onSuccess = data, data = null;
        }

        /* method type */
        if (type == 'GET') req.query(data);
        if (type == 'POST') req.send(data);
        if (type == 'PUT') req.send(data);
        if (type == 'PATCH') req.send(data);

        /* kind of request */
        req.type('json');

        /* if onSuccess function exists */
        if(onSuccess){
            req.end((err, response) => {
                response = response || {};
                if(!err) onSuccess(response);
                if(err) onError(response);
            });
        }
        
        return req;
    },

    get(url, data, onSuccess, onError){
        return this._request('GET', url, data, onSuccess, onError);
    },

    post(url, data, onSuccess, onError){
        return this._request('POST', url, data, onSuccess, onError);
    },

    put(url, data, onSuccess, onError){
        return this._request('PUT', url, data, onSuccess, onError);
    },

    delete(url, onSuccess, onError){
        return this._request('DELETE', url, onSuccess, onError);
    }
};