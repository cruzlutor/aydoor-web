/* jslint esnext: true */
import validator from 'validator';

validator.extend('isRequired', function (str) {
    return str !== '';
});

export default validator;