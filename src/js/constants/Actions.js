/* jslint esnext:true */

export default {
    CHANGE: 'CHANGE',
    ERROR: 'ERROR',
    
    // auth actions
    SIGNIN: 'SIGNIN',
    SIGNIN_ERROR : 'SIGNIN_ERROR',

    SIGNOUT: 'SIGNOUT',
    SIGNOUT_ERROR: 'SIGNOUT_ERROR',

    SIGNUP: 'SIGNUP',
    SIGNUP_ERROR: 'SIGNUP_ERROR',

    // user actions
    UPDATE_PROFILE: 'UPDATE_PROFILE',
    UPDATE_PROFILE_ERR: 'UPDATE_PROFILE_ERR',
};