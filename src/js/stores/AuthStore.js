/* jslint esnext:true */
import {EventEmitter} from 'events';
import store from 'store';
import Dispatcher from './../dispatcher/Dispatcher';
import Actions from './../constants/Actions';

let _error = {};

let _state = {
    isLoggedIn: false,
    email: null,
    avatar: null,
    token: null,
    id: null,
};

class AuthStore extends EventEmitter {

    isLoggedIn() {
        return _state.isLoggedIn;
    }

    getToken(){
        return _state.token;
    }

    getError(){
        return _error;
    }
}

let _AuthStore = new AuthStore();

Dispatcher.register((playload) => {

    switch (playload.actionType) {

        /**
         * Login event
         * @type {string}
         */
        case Actions.SIGNIN:
            console.log('login!');
            _state.isLoggedIn = true;
            _state.token = playload.token;

            // set store data
            store.set('token', _state.token);
            _AuthStore.emit(Actions.SIGNIN);
            break;

        /**
         * Login error event
         * @type {string}
         */
        case Actions.SIGNIN_ERROR:
            _error = playload.data;
            _AuthStore.emit(Actions.SIGNIN_ERROR);
            break;

        /**
         * Logout event
         * @type {string}
         */
        case Actions.SIGNOUT:
            _state.isLoggedIn = false;
            _state.token = null;

            // remove store data
            store.remove('token');
            _AuthStore.emit(Actions.SIGNOUT);
            break;
    }
});

export default _AuthStore;