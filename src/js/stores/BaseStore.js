/* jslint esnext:true */
import {EventEmitter} from 'events';

const CHANGE_EVENT = 'change';
const ERROR_EVENT = 'error';

export default class BaseStore extends EventEmitter {

    // emitChange() {
    //     this.emit(CHANGE_EVENT);
    // }

    // addChangeListener(cb) {
    //     this.on(CHANGE_EVENT, cb);
    // }

    // removeChangeListener(cb) {
    //     this.removeListener(CHANGE_EVENT, cb);
    // }

    // emitError() {
    //     this.emit(ERROR_EVENT);
    // }

    // addErrorListener(cb) {
    //     this.on(ERROR_EVENT, cb);
    // }

    // removeErrorListener(cb) {
    //     this.removeListener(ERROR_EVENT, cb);
    // }
}