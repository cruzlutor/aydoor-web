/* jslint esnext: true */

/**
 *  Full example phrases airbnb polyglot
 * 
 *  car: 'Carro',
 *  car.-p: 'Carros',
 *  car._c: '%{smart_count} Carro |||| %{smart_count} Carros',
 *  car.door: 'Puerta del carro',
 *  car.owner: 'El carro es de %{owner}',
 *  
 */

export default {

    'hello': 'hola',
    'hello.name': 'hola, %{name}',

    'first_name': 'Nombre',
    'last_name': 'Apellido',

    'home': 'Inicio',
    'home.slogan': 'Encuentra clases e instructores a domicilio',
    'home.service': '¿Qué servicio buscas?',
    'home.city': '¿En qué ciudad estas?',

    'search': 'Buscar',

    'sign_up': 'Registrate',
    'sign_up.city': '¿En qué ciudad vives?',

    'sign_in': 'Iniciar sesión',

    'sign_out': 'Salir',
    
    'validator.isRequired': 'Este campo es requerido',
    'validator.isEmail': 'Ingrese un email correcto',
    'validator.isAlpha': 'El campo solo debe contener letras',

    'message': 'Mensaje',
    'message._p': 'Mensajes',

    'appointment': 'Cita',
    'appointment._p': 'Citas',

    'service': 'Servicio',
    'service._p': 'Servicios',

    'help': 'Ayuda',

    'dashboard': 'Dashboard',
    
    'profile.my': 'Mi perfil',
    'profile.edit': 'Editar perfil',

    'account': 'Cuenta',
    'account.my': 'Mi cuenta',

    'payment': 'Pago',
    'payment.settings': 'Preferencias de pago',

    'settings': 'Ajustes',

    'configure': 'Configurar',

    'review': 'Opinion',
    'review._p': 'Opiniones',
    'review.comments': 'Opiniones y comentarios',

    'email' : 'Email',
    'email.change' : 'Cambiar email',

    'password' : 'Password',
    'password.change' : 'Cambiar password',

    'change': 'Cambiar',

    'gender': 'Genero',

    'birth_date': 'Fecha de nacimiento',

    'about': 'Acerca',
    'about.you': 'Acerca de ti',
    
};