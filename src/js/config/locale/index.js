/* jslint esnext: true */
import Polyglot from 'node-polyglot';
import es from './es';

export default new Polyglot({phrases: es, locale: "es"});